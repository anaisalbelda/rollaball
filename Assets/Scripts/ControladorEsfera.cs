﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorEsfera : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    private int count;
    public Text text;
    public Text winText;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        updateCounter();
        winText.text = ""; 
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 direccion = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(direccion * speed);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "pickup") {
            Destroy(other.gameObject);
            count++;
            updateCounter();
        }
    }

    void updateCounter()
    {
        text.text = "Puntos: " + count;

        int numPickups = GameObject.FindGameObjectsWithTag("pickup").Length;

        if (numPickups == 1) {
            winText.text = "Has Ganado!";
        }
    }
}